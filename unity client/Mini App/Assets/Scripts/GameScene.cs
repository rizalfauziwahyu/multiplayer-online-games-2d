﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RestClient.Core;
using RestClient.Core.Models;

public class GameScene : MonoBehaviour
{
    [SerializeField] private GameObject playerOne;
    [SerializeField] private GameObject playerTwo;

    [SerializeField]
    string baseUrl = "";

    objectGame objects = new objectGame { };

    public GameObject alertBox;
    public Text alert;

    public void sendData()
    {
        RequestHeader contentTypeHeader = new RequestHeader
        {
            Key = "Content-Type",
            Value = "application/json"
        };

        StartCoroutine(RestWebClient.Instance.HttpPost(baseUrl, JsonUtility.ToJson(objects), (r) => alertLogIn(r), new List<RequestHeader>
        {
            contentTypeHeader
        }));
    }

    void alertLogIn(Response response)
    {
        Debug.Log($"Status Code: {response.StatusCode}");
        Debug.Log($"Data: {response.Data}");
        Debug.Log($"Error: {response.Error}");

        alertBox.SetActive(true);
        alert.text = response.Data;
    }

    public void closeAlert()
    {
        alertBox.SetActive(false);
    }

    private void Awake()
    {
        SpriteRenderer playerOneRenderer = playerOne.GetComponent<SpriteRenderer>();
        playerOneRenderer.color = GetColorFromString("16FF00");
        objects.color = GetStringFromColor(playerOneRenderer.color);


        SpriteRenderer playerTwoRenderer = playerTwo.GetComponent<SpriteRenderer>();
        playerTwoRenderer.color = GetColorFromString("001EFF");
        objects.color2 = GetStringFromColor(playerTwoRenderer.color);

    }

    private void Update()
    {
        objects.coordinateX = playerOne.GetComponent<Transform>().position.x;
        objects.coordinateY = playerOne.GetComponent<Transform>().position.y;

        objects.coordinateX2 = playerTwo.GetComponent<Transform>().position.x;
        objects.coordinateY2 = playerTwo.GetComponent<Transform>().position.y;
    }

    private int HexToDec(string hex)
    {
        int dec = System.Convert.ToInt32(hex, 16);
        return dec;
    }

    private string DecToHex(int value)
    {
        return value.ToString("X2");
    }

    private string FloatNormalizedToHex(float value)
    {
        return DecToHex(Mathf.RoundToInt(value * 255f));
    }

    private float HexToFloatNormalized(string hex)
    {
        return HexToDec(hex) / 255f;
    }

    private Color GetColorFromString(string hexString)
    {
        float red = HexToFloatNormalized(hexString.Substring(0, 2));
        float green = HexToFloatNormalized(hexString.Substring(2, 2));
        float blue = HexToFloatNormalized(hexString.Substring(4, 2));
        float alpha = 1f;
        if(hexString.Length >= 8)
        {
            alpha = HexToFloatNormalized(hexString.Substring(6, 2));
        }
        return new Color(red, green, blue, alpha);
    }

    private string GetStringFromColor(Color color, bool useAlpha = false)
    {
        string red = FloatNormalizedToHex(color.r);
        string green = FloatNormalizedToHex(color.g);
        string blue = FloatNormalizedToHex(color.b);
        if(!useAlpha)
        {
            return red + green + blue;
        } else
        {
            string alpha = FloatNormalizedToHex(color.a);
            return red + green + blue + alpha;
        }
    }
}

[SerializeField]
public class objectGame
{
    public string color;
    public float coordinateX;
    public float coordinateY;

    public string color2;
    public float coordinateX2;
    public float coordinateY2;
}
