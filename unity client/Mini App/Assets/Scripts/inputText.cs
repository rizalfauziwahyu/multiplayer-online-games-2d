﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace client
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    public class sampleTcpUdpClient2
    {
        public enum clientType
        {
            TCP,
            UDP
        } //Type of connection the client is making.

        private const int ANYPORT = 0;

        private const int SAMPLETCPPORT = 4567;

        private const int SAMPLEUDPPORT = 4568;

        private bool readData = false;

        public clientType cliType;

        private bool DONE = false;

        public string a;

        public sampleTcpUdpClient2()
        {
            this.cliType = clientType.TCP;
        }

        public void sampleTcpClient2(String serverName, String whatEver)
        {
            try
            {
                //Create an instance of TcpClient.
                TcpClient tcpClient = new TcpClient(serverName, SAMPLETCPPORT);

                //Create a NetworkStream for this tcpClient instance.~
                //This is only required for TCP stream.
                NetworkStream tcpStream = tcpClient.GetStream();
                if (tcpStream.CanWrite)
                {
                    Byte[] inputToBeSent =
                        System
                            .Text
                            .Encoding
                            .ASCII
                            .GetBytes(whatEver.ToCharArray());
                    tcpStream.Write(inputToBeSent, 0, inputToBeSent.Length);
                    tcpStream.Flush();
                }
                while (tcpStream.CanRead && !DONE)
                {
                    //We need the DONE condition here because there is possibility that
                    //the stream is ready to be read while there is nothing to be read.
                    if (tcpStream.DataAvailable)
                    {
                        Byte[] received = new Byte[512];
                        int nBytesReceived =
                            tcpStream.Read(received, 0, received.Length);
                        String dataReceived =
                            System.Text.Encoding.ASCII.GetString(received);
                        Console.WriteLine(dataReceived);
                        a = dataReceived;
                        DONE = true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An Exception has occurred.");
                Console.WriteLine(e.ToString());
            }
        }

        public void sampleUdpClient2(String serverName, String whatEver)
        {
            try
            {
                //Create an instance of UdpClient.
                UdpClient udpClient = new UdpClient(serverName, SAMPLEUDPPORT);
                Byte[] inputToBeSent = new Byte[256];
                inputToBeSent =
                    System.Text.Encoding.ASCII.GetBytes(whatEver.ToCharArray());
                IPHostEntry remoteHostEntry = Dns.GetHostByName(serverName);
                IPEndPoint remoteIpEndPoint =
                    new IPEndPoint(remoteHostEntry.AddressList[0],
                        SAMPLEUDPPORT);
                int nBytesSent =
                    udpClient.Send(inputToBeSent, inputToBeSent.Length);
                Byte[] received = new Byte[512];
                received = udpClient.Receive(ref remoteIpEndPoint);
                String dataReceived =
                    System.Text.Encoding.ASCII.GetString(received);
                Console.WriteLine(dataReceived);
                udpClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("An Exception Occurred!");
                Console.WriteLine(e.ToString());
            }
        }

    }

    public class inputText : MonoBehaviour
    {
        public GameObject inputField;
        public string insertText;
        public GameObject showText;

        public void sendText()
        {
            insertText = inputField.GetComponent<Text>().text;

            sampleTcpUdpClient2 stc = new sampleTcpUdpClient2();
            stc.sampleTcpClient2("localhost", insertText);

            showText.GetComponent<Text>().text = stc.a;
        }

    }

}