# FroggyJump Multiplayer Online Games 2D Unity and Colyseus Framework

### Framework
#### [Express](https://docs.nestjs.com/)
Fast, unopinionated, minimalist web framework for Node.js

#### [Colyseus](https://docs.colyseus.io/)
![alt-text](https://static.wixstatic.com/media/1ec665_618db3719b6e4602b43cb77bdbb39a91~mv2.png/v1/fill/w_520,h_138,al_c,q_85,usm_0.66_1.00_0.01/1200_Left-Main.web)<br>
An open-source multiplayer solution that empowers game devs to Get To Fun Faster™. Built on Node.js

### Database
#### [Mongodb](https://mongodb.com)
Mongodb is general purpose, document based, distributed database built for modern application developers and for the cloud era.

### Authentication
#### [JWT](https://jwt.io/)
JWT works best for single use tokens. Ideally, a new JWT must be generated for each use.

### Diagram
#### Our Deployment Diagram
![Screen Shot](documentation/deploymentdiagram.jpg)

#### Froggy Jump Diagram
![Screen Shot](documentation/froggyjumpdiagram.jpg)

## Get Started

### Precondition
* Node.js (> = 8.9.0)
* Unity

#### <i>Clone</i> Project App

git

```bash
https://gitlab.com/rizalfauziwahyu/multiplayer-online-games-2d.git
```

#### <i>Setup Colyseus Server</i>
moving to colyseus-server branch

```bash
git checkout colyseus-server
```

go to server directory

```bash
cd ./server
```

install dependencies

```bash
npm install
```

running server

```bash
npm start
```

#### <i>Setup Unity Client</i>
moving to colyseus-framework branch

```bash
git checkout colyseus-framework
```

go to server directory

```bash
cd ./"Froggies Jump"/
```

Add "Froggies Jump" directory to your unity hub and run project.


## Game Overview

A multiplayer platform race game online that can include 2 to 30 players at the same times. Being an accomplished climber is a matter of pride for the residents of Froggy Jump. Therefore, there are often sudden challenges on every side of the city to challenge the Froggy Jump residents who claim to be master climbers. Competing to reach the highest peak may sound very easy, but not until obstacles and obstacles await the climbers in the middle of the race session. If one of the residents can successfully win the race then he is a real master climber.

## Game Detail

1. Genre: MMORPG, Casual, Trivia
2. Network Protocol: http, websocket (TCP)
3. Platform: Windows

## Game Screenshots
##### Login
![alt-text](documentation/login.gif)

##### Main Menu
![alt-text](documentation/menu.gif)

##### Gameplay
![alt-text](documentation/gameplay.gif)

##### Gameover
![alt-text](documentation/gameover.gif)



## Development Team
1. (Author) 4210181005 Muhammad Rizal Fauzi Wahyu P.
2. (Author) 4210181008 Zsalsabilla Pasya E.
3. (Course) Praktikum Desain Multiplayer Game Online
