﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using RestClient.Core;
using RestClient.Core.Models;


public class LogInScene : MonoBehaviour
{
    public GameObject inputUsn;
    public GameObject inputPass;
    public GameObject alertBox;
    [SerializeField]
    string baseUrl = "https://753d946c5e35.ngrok.io/api/users/authenticate";
    public Text alert;

    public void logIn()
    {
        RequestHeader contentTypeHeader = new RequestHeader
        {
            Key = "Content-Type",
            Value = "application/json"
        };

        Player player = new Player { username = "fauzi1", password = "fauzi1" };
        /*player.username = inputUsn.GetComponent<Text>().text;
        player.password = inputPass.GetComponent<Text>().text;*/

        //Debug.Log(JsonUtility.ToJson(player));
        StartCoroutine(RestWebClient.Instance.HttpPost(baseUrl, JsonUtility.ToJson(player), (r) => alertLogIn(r), new List<RequestHeader>
        {
            contentTypeHeader
        }));
    }

    void alertLogIn(Response response)
    {
        Debug.Log($"Status Code: {response.StatusCode}");
        Debug.Log($"Data: {response.Data}");
        Debug.Log($"Error: {response.Error}");

        alertBox.SetActive(true);
        alert.text = response.Data;

    }

    public void nextScene()
    {
        SceneManager.LoadScene("MapScene");
    }
}

[SerializeField]
public class Player
{
    public string username;
    public string password;
}
